/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
	        2. Delete those files simultaneously 
*/
const fs = require("fs");


function createDeleteFile(callBack) {

  fs.mkdir("randomDirectory", (err) => {

    if (err) {

      callBack(err, null)

    } else {

      callBack(null, "Directory has been created.")
      let completeExcutions = 0
      let completeExcutionsDel = 0
      let fileNames = []
      for (let index = 0; index < 4; index++) {
        let data = [{
            name: "Molecule Man",
            age: 29,
            secretIdentity: "Dan Jukes",
          },
          {
            name: "Madame Uppercut",
            age: 39,
            secretIdentity: "Jane Wilson",
          }, {
            "fileNumber": index
          }
        ];
        let fileName = `./randomDirectory/randomFile${index}.json`

        fs.writeFile(fileName, JSON.stringify(data), (err) => {
          if (err) {

            callBack(err, null)

          } else {
            completeExcutions += 1

            fileNames.push(`files${index + 1}`)

            if (completeExcutions == 4) {

              callBack(null, "All File has been created.")
            }
            fs.unlink(fileName, (err) => {
              if (err) {
                callBack(err, null)
              } else {
                completeExcutionsDel += 1
                if (completeExcutionsDel == fileNames.length) {
                  callBack(null, "All Randoms files deleted.")
                }
              }
            })
          }
        })
      }
    }
  })
}

module.exports = createDeleteFile