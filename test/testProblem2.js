const problem2 = require('../problem2')

const callBack = (err, data) => {
    if (err) {
        console.log(err)
    } else {
        console.log(data)
    }
}

problem2(callBack)