const problem1 = require("../problem1")

const callBack = (err, data) => {
    if (err) {
        console.log(err)
    } else {
        console.log(data)
    }
}

problem1(callBack)