/*
1. Read the given file lipsum.txt
2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");

function deleteFileFromList(callBack) {
  fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
    if (err) {
      callBack(err, null)
    } else {
      callBack(null, "File has been readed.")
      fs.writeFile("uppercaseFile.txt", data.toUpperCase(), (err) => {
        if (err) {
          callBack(err, null)
        } else {
          callBack(null, "all letter converted in uppercase converted.")
          fs.writeFile("filenames.txt", "uppercaseFile.txt", (err) => {
            if (err) {
              callBack(err, null)
            } else {
              callBack(null, "UppercaseFile name added to filesaname.txt")
              fs.readFile("uppercaseFile.txt", "utf-8", (err, data1) => {
                if (err) {
                  callBack(err, null)
                } else {
                  callBack(null, "Uppercase file read.")
                  let lowercaseFileData = data1.toLowerCase().split(".").join("/n")
                  fs.writeFile("lowercaseFile.txt", lowercaseFileData, (err) => {
                    if (err) {
                      callBack(err, null)
                    } else {
                      callBack(null, "Converted into lowercase file and split it.")
                      fs.appendFile("filenames.txt", "\nlowercaseFile.txt", (err) => {
                        if (err) {
                          callBack(err, null)
                        } else {
                          callBack(null, "lowercasefile name append to filenames.txt.")
                          fs.readFile("lowercaseFile.txt", "utf-8", (err, data2) => {
                            if (err) {
                              callBack(err, null)
                            } else {
                              let sorteContent = data2.split("\n").sort().join("/n")
                              fs.writeFile("sortedContent.txt", sorteContent, (err) => {
                                if (err) {
                                  callBack(err, null)
                                } else {
                                  callBack(null, "sorted content added to new a files.")
                                  fs.appendFile("filenames.txt", "\nsortedContent.txt", (err) => {
                                    if (err) {
                                      callBack(err, null)
                                    } else {
                                      callBack(null, "sorted content filename added to filenames.")
                                      fs.readFile("filenames.txt", "utf-8", (err, data3) => {
                                        if (err) {
                                          callBack(err, null)
                                        } else {
                                          let fileContent = data3.split("\n");
                                          fileContent.forEach((element) => {
                                            fs.unlink(element, (err) => {
                                              if (err) {
                                                callBack(err, null)
                                              } else {
                                                callBack(null, "all files deleted.")
                                              }
                                            });
                                          });
                                        }
                                      })
                                    }
                                  })
                                }
                              })
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  })
}

module.exports = deleteFileFromList